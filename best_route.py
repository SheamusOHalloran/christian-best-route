"""This programs find the shortest path in hour between two aiports"""

import heapq
import sys

ERROR_MESSAGE = '\nThis program needs departure and destination ' + \
                'airport codes as arguments. E.g.: DUB SYD\n'


class Airport:
    """ Airport domain class models an airport."""

    def __init__(self, code):
        self.code = code
        self.adjacent = {}
        # Set initial distance to a huge int to simulate infinite
        self.distance = 9999999999
        # Mark all nodes unvisited
        self.visited = False
        # Predecessor
        self.previous = {}

    def __lt__(self, other):
        """Implement the < operator for the Airport instance"""
        return self.distance < other.distance

    def add_neighbor(self, neighbor, weight=0):
        """Add a neighbor airport to the list"""
        self.adjacent[neighbor] = weight

    def get_connections(self):
        """Get connections of the airport"""
        return self.adjacent.keys()

    def get_code(self):
        """Returnn the airport code"""
        return self.code

    def get_weight(self, neighbor):
        """Return the time to the adjacent airport"""
        return self.adjacent[neighbor]

    def set_distance(self, dist):
        """Set the distance to an compared airport"""
        self.distance = dist

    def get_distance(self):
        """Get the distance to an compared airport"""
        return self.distance

    def set_previous(self, prev):
        """Get a previous airport"""
        self.previous = prev

    def set_visited(self):
        """Set visited attribute to true"""
        self.visited = True


class FlightMap:
    """ The FlightMap class represent a Graf of airports and roads between them"""

    def __init__(self):
        self.airport_dict = {}
        self.num_airports = 0
        self.previous = None

    def __iter__(self):
        return iter(self.airport_dict.values())

    def add_airport(self, node):
        """Add an airport instance to the map"""
        self.num_airports = self.num_airports + 1
        new_airport = Airport(node)
        self.airport_dict[node] = new_airport
        return new_airport

    def get_airport_from_code(self, code):
        """Return the airport correspoonding the passed airport code if exists"""
        if code in self.airport_dict:
            return self.airport_dict[code]


    def add_road(self, from_airport, to_airport, time = 0):
        """Add a new road between to airports"""
        if from_airport not in self.airport_dict:
            self.add_airport(from_airport)
        if to_airport not in self.airport_dict:
            self.add_airport(to_airport)
        self.airport_dict[from_airport].add_neighbor(self.airport_dict[to_airport], time)
        self.airport_dict[to_airport].add_neighbor(self.airport_dict[from_airport], time)

    def get_airports(self):
        """Get all airport keys from the airport dictionary"""
        return self.airport_dict.keys()

    def set_previous(self, current_airport):
        """Set the previous airport"""
        self.previous = current_airport

    def get_previous(self):
        """Return the previous airport"""
        return self.previous


def get_best_route(airport, path, total = 0, routes = ''):
    """find the shortest path from previous airport"""
    local_path = path
    if airport.previous:
        local_path.append(airport.previous.get_code())
        total, routes = get_best_route(airport.previous, local_path)
        route = airport.previous.get_code() + ' -- ' + airport.get_code() \
            + ' ( ' + str(airport.get_weight(airport.previous)) + ' )'
        routes = routes + '\n' + route
        total = airport.get_weight(airport.previous) + total
    return total, routes


def calculate_best_route(m_flight_map, start):
    """Based on Dijkstra's algorithm"""
    start.set_distance(0)

    # Putting routes in a priority queue
    unvisited_pq = [(airport.get_distance(), airport) for airport in m_flight_map]
    heapq.heapify(unvisited_pq)

    while len(unvisited_pq):
        # Select the airport with the smallest distance from neightbourghood airport
        unvisited_airport = heapq.heappop(unvisited_pq)
        current_airport = unvisited_airport[1]
        current_airport.set_visited()

        for next_airport in current_airport.adjacent:

            if next_airport.visited:
                continue

            new_distance = current_airport.get_distance() + current_airport.get_weight(next_airport)

            if new_distance < next_airport.get_distance():
                next_airport.set_distance(new_distance)
                next_airport.set_previous(current_airport)

        # Rebuild priority queue
        while len(unvisited_pq):
            heapq.heappop(unvisited_pq)
        unvisited_pq = [(airport.get_distance(), airport) \
            for airport in m_flight_map if not airport.visited]
        heapq.heapify(unvisited_pq)


def build_flight_map():
    """Build the map of airports and roads"""
    m_flight_map = FlightMap()
    m_flight_map.add_airport('DUB')
    m_flight_map.add_airport('LHR')
    m_flight_map.add_airport('CDG')
    m_flight_map.add_airport('BOS')
    m_flight_map.add_airport('BKK')
    m_flight_map.add_airport('ORD')
    m_flight_map.add_airport('LAS')
    m_flight_map.add_airport('NYC')
    m_flight_map.add_airport('LAX')
    m_flight_map.add_airport('SYD')
    m_flight_map.add_road('DUB', 'LHR', 1)
    m_flight_map.add_road('DUB', 'CDG', 2)
    m_flight_map.add_road('CDG', 'BOS', 6)
    m_flight_map.add_road('CDG', 'BKK', 9)
    m_flight_map.add_road('ORD', 'LAS', 2)
    m_flight_map.add_road('LHR', 'NYC', 5)
    m_flight_map.add_road('NYC', 'LAS', 3)
    m_flight_map.add_road('BOS', 'LAX', 4)
    m_flight_map.add_road('LHR', 'BKK', 9)
    m_flight_map.add_road('BKK', 'SYD', 11)
    m_flight_map.add_road('LAX', 'LAS', 2)
    m_flight_map.add_road('DUB', 'ORD', 6)
    m_flight_map.add_road('LAX', 'SYD', 13)
    m_flight_map.add_road('LAS', 'SYD', 14)
    return m_flight_map


def check_number_of_arguments():
    """Verify if the number of passed arguments is correct"""
    if len(sys.argv) != 3:
        print(ERROR_MESSAGE)
        raise Exception('Illegal number of arguments: {0}'.format(len(sys.argv) - 1))


def check_arguments_fit_airport_codes(flight_map, departure_code, destination_code):
    """Verify if the two string arguments are in the list of airport codes"""
    airport_exists = True

    if not flight_map.get_airport_from_code(departure_code):
        print('\n{} airport code does not exists'.format(departure_code))
        airport_exists = False

    if not flight_map.get_airport_from_code(destination_code):
        print('\n{} airport code does not exists'.format(destination_code))
        airport_exists = False

    if not airport_exists:
        print(ERROR_MESSAGE)
        raise Exception('Possible airport codes: {0}\n'.format(flight_map.get_airports()))


def start_process(departure_code, destination_code):
    """Return the best route and the total time in hour"""
    departure_code = departure_code.upper()
    destination_code = destination_code.upper()

    flight_map = build_flight_map()
    check_arguments_fit_airport_codes(flight_map, departure_code, destination_code)

    departure = flight_map.get_airport_from_code(departure_code)
    destination = flight_map.get_airport_from_code(destination_code)

    calculate_best_route(flight_map, departure)

    return get_best_route(destination, [destination.get_code()])



if __name__ == '__main__':
    try:
        check_number_of_arguments()
        departure_code = sys.argv[1]
        destination_code = sys.argv[2]
        final_total, final_routes = start_process(departure_code, destination_code)
        routes_arr = final_routes.split('\n')
        print(final_routes)
        print('time: {0}\n'.format(str(final_total)))
    except Exception as e:
        print(str(e))
        sys.exit()
