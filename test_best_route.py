from bestroute.best_route import *
import pytest


def test_get_best_route_happy_cenario_should_return_correct_route():
    '''Tests the happy cenario'''

    # Arrange
    departure_code = 'DUB'
    destination_code = 'SYD'

    # Act
    final_total, final_routes = start_process(departure_code, destination_code)

    # Assert
    assert final_total == 21
    routes_arr = final_routes.split('\n')
    assert routes_arr[1] == 'DUB -- LHR ( 1 )'
    assert routes_arr[2] == 'LHR -- BKK ( 9 )'
    assert routes_arr[3] == 'BKK -- SYD ( 11 )'


def test_get_best_route_happy_cenario_with_lower_cased_codes_should_return_correct_route():
    '''Tests the happy cenario'''

    # Arrange
    departure_code = 'dub'
    destination_code = 'syd'

    # Act
    final_total, final_routes = start_process(departure_code, destination_code)

    # Assert
    assert final_total == 21
    routes_arr = final_routes.split('\n')
    assert routes_arr[1] == 'DUB -- LHR ( 1 )'
    assert routes_arr[2] == 'LHR -- BKK ( 9 )'
    assert routes_arr[3] == 'BKK -- SYD ( 11 )'


def test_get_best_route_with_wrong_departure_code_should_throw_exception():
    with pytest.raises(Exception):
        # Arrange
        departure_code = 'DoB'
        destination_code = 'SYD'

        # Act
        start_process(departure_code, destination_code)


def test_get_best_route_with_wrong_destination_code_should_throw_exception():
    with pytest.raises(Exception):
        # Arrange
        departure_code = 'DUB'
        destination_code = 'SiD'

        # Act
        start_process(departure_code, destination_code)