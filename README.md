# README #

The program contained in the file 'best_route.py' find the shortest path in hour between two aiports. It is based on Dijkstra's shortest path algorithm. It is a python3 program.

## Steps to get the ' best route' program up and running from the CLI:

* First of all, you need to have [Python 3.7 installed](https://www.python.org/downloads/release/python-370/) on your machine 
* Change directory to where you unzipped the containing zip file and where the file 'best_route.py' is
* Then, use the following command to run the python 3 program:

```python3 best_route.py <DEPARTURE_AIRPORT_CODE> <ARRIVAL_AIRPORT_CODE>```

"python3" can be "python" or another syntax depending on your python configuration.

The terms '<DEPARTURE_AIRPORT_CODE>' and '<ARRIVAL_AIRPORT_CODE>' must be substitued by an actual airport code from the following list:

* DUB -> Dublin International Airport
* LHR -> London Heathrow International Airport
* CDG -> Paris Charles de Gaulle International Airport
* BOS -> Boston Logan International Airport
* BKK -> Bangkok Suvarnabhumi International Airport
* ORD -> Chicago O'Hare International Airport
* LAS -> Las Vegas McCarran International Airport
* NYC -> New York City John F. Kennedy International Airport
* LAX -> Los Angeles International Airport
* SYD -> Sydney International Airport

## How to run unit tests from the CLI:

* Install pytest run the following command: ```pip3 install pytest```
* Change directory to where you unzipped the containing zip file and where the file 'test_best_route.py' is
* Execute the following command to run pytest: ```pytest```